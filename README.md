# Docker Images

This repo contains dockerfiles used to create FPGA, UHD, and Gnuradio test environments.

## Vivado

The [Vivado](./vivado) folder contains dockerfiles for various vivado
versions. Nominal support right now is vivado 2017.4. Note that the
appropriate vivado *installer* needs to be downloaded from the Xilinx
website and located in the corresponding subdirectory in order to create
the docker image.

Vivado is installed directly into the image for ease-of-use, but note this
creates overly large images! Vivado 2017.4 is, I believe, something like 40 GB
and that does not even include bigger devices like Ultrascales.

## UHD & Gnuradio

We provide here developer-centric images for both UHD and Gnuradio. These
images may be used in a test environment (for CI/CD) to efficiently
pull an image that includes *only* the required dependencies -- this
significantly improves testing time and reliability for out-of-tree (OOT) repos
targetting either UHD or Gnuradio software development.

Because these are developer-facing images, each image builds the relevant
repos from source. We also try to capture plausible permutations of
dependencies that developers would be interested in. Some of the images are
versioned release tags, which are relevant to support, and some of the images
are built directly from specific branches (e.g., master, maint-3.7, UHD-3.13)
to capture compatibility with long-lived release-branches.

Gitlab CI is used to build and push docker images on an automatic basis; all
branch-based builds are run weekly in case of new code. Tag-based builds are
run on-demand and generally only need to be initiated once.

#### UHD

UHD includes a build script ([build-branch-rfnoc-all.sh](./uhd-build/build-branch-rfnoc-all.sh)) which is used to create Docker image builds.

Syntax example:

`./build-branch-rfnoc-all.sh <image-name> <uhd-branch>`

The image-name represents the output docker image name to build (this can be changed later using the `docker tag` command).

Uhd-branch indicates a relevant branch, tag, or commit sha to checkout when cloning and building source code. The uhd-branch will be inserted into the docker image tag when complete.

#### Gnuradio

Similar to UHD, gnuradio builds may also be created from various source
commits, branches, tags, etc. The gnuradio image is built on top of an
existing UHD docker image from the "uhd-build" folder.

The build script syntax is slightly more involved since it also needs to
specify which UHD image to build from:

`./build-gnuradio-maint-3.7.sh <image-name> <uhd-base-image> <gnuradio-branch>`

Again, image-name represents output docker image name. The gnuradio-branch
indicates the relevant branch of gnuradio source code to use. Finally, the
uhd-base-image argument tells the build script *which* UHD image to use as
a dependency.
