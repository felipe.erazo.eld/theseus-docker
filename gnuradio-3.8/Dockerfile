FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    git wget zip unzip \
    cmake build-essential git-core cmake g++ pkg-config \
    libboost-all-dev \
    libgmp3-dev \
    libfftw3-3 \
    libfftw3-dev \
    libcppunit-dev \
    swig3.0 \
    libgsl-dev \
    libasound2-dev \
    python3-dev \
    python3-numpy \
    python3-yaml \
    python3-mako \
    python3-lxml \
    python3-gi \
    python3-gi-cairo \
    python3-requests \
    gir1.2-gtk-3.0 \
    python3-cairo-dev \
    qtbase5-dev \
    pyqt5-dev  \
    pyqt5-dev-tools \
    libqwt-qt5-dev \
    liblog4cpp5-dev \
    libzmq3-dev \
    python3-zmq \
    python3-click \
    python3-click-plugins \
    python3-setuptools \
    x11-apps \
&& rm -rf /var/lib/apt/lists/*

# Build Info
ARG UHD_COMMIT=UHD-3.15.LTS
ARG GR_COMMIT=maint-3.8
ARG ETTUS_COMMIT=maint-3.8
ARG MAKE_THREADS=2

# UHD cmake defaults (overrideable)
ARG UHD_ENABLE_RFNOC=1
ARG UHD_ENABLE_B100=0
ARG UHD_ENABLE_B200=1
ARG UHD_ENABLE_USRP2=1
ARG UHD_ENABLE_X300=1
ARG UHD_ENABLE_MPMD=1
ARG UHD_ENABLE_N300=1
ARG UHD_ENABLE_E320=1
ARG UHD_ENABLE_EXAMPLES=1
ARG UHD_ENABLE_UTILS=1

WORKDIR /home/root
COPY install-*.sh ./
RUN ./install-uhd.sh
RUN ./install-gnuradio.sh
RUN ./install-gr-ettus.sh

# Create a user
RUN adduser --disabled-password --gecos '' developer
USER developer
WORKDIR /home/developer
